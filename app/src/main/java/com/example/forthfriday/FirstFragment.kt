package com.example.forthfriday

import android.os.Bundle
import android.util.Log.i
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.forthfriday.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init(){
        binding.btnStartGame.setOnClickListener {
            checkNum()
        }
    }

    private fun checkNum(){
        val cubeNum = binding.edTCubeNum.text.toString()
        if (cubeNum.isNotEmpty() && (cubeNum.toInt() == 9 || cubeNum.toInt() == 16 || cubeNum.toInt() == 25)){
            val bundle = bundleOf("cubeNum" to cubeNum.toInt())
            findNavController().navigate(R.id.action_firstFragment_to_secondFragment, bundle)
        }else{
            Toast.makeText(activity, "Please enter 9, 16 or 25", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}