package com.example.forthfriday

import android.os.Bundle
import android.text.Layout
import android.util.Log.i
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.forthfriday.databinding.FragmentSecondBinding

class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null

    private val binding get() = _binding!!

    private val  numbers: MutableList<Int> = mutableListOf()

    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init(){
        addNum()

        adapter = RecyclerViewAdapter(numbers)
        val cubeNum = arguments?.getInt("cubeNum", 9)
        cubeNum?.let {
            when (cubeNum) {
                9 -> {
                    binding.recyclerView.layoutManager = GridLayoutManager(activity, 3)
                }
                16 -> {
                    binding.recyclerView.layoutManager = GridLayoutManager(activity, 4)
                }
                25 -> {
                    binding.recyclerView.layoutManager = GridLayoutManager(activity, 5)
                }
            }
        }
        binding.recyclerView.adapter = adapter

    }

    private fun addNum(){
        val cubeNum = arguments?.getInt("cubeNum", 9)
        cubeNum?.let {
            for (i in 1..cubeNum.toInt()){
                numbers.add(i)
            }
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}