package com.example.forthfriday

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.forthfriday.databinding.TacLayoutBinding

class RecyclerViewAdapter(private val numbers: MutableList<Int>): RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView = TacLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ItemViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = numbers.size

    inner class ItemViewHolder(private val binding: TacLayoutBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(){
            val num = numbers[adapterPosition]
            binding.imgClick.setOnClickListener{
                if (num%2 == 1){
                    binding.imgClick.setImageResource(R.drawable.x_icon)
                    binding.imgClick.isClickable = false
                }else{
                    binding.imgClick.setImageResource(R.drawable.zero_icon)
                    binding.imgClick.isClickable = false
                }
            }
        }

    }
}